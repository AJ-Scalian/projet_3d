function updateDatDropdown(target, list, val){
    innerHTMLStr = "";
    if(list.constructor.name == 'Array'){
        for(var i=0; i<list.length; i++){
            let sel = list[i] == val ? "selected" : "";
            var str = "<option value='" + list[i] + "' " + sel + ">" + list[i] + "</option>";
            innerHTMLStr += str;
        }
    }

    if(list.constructor.name == 'Object'){
        for(var key in list){
            let sel = list[key] == val ? "selected" : "";
            var str = "<option value='" + list[key] + "' " + sel + ">" + key + "</option>";
            innerHTMLStr += str;
        }
    }
    if (innerHTMLStr != "") target.domElement.children[0].innerHTML = innerHTMLStr;
}

function lonLatToVector3( lon, lat,radius)
{
    var phi   = (90-lat)*(Math.PI/180),
        theta = (lon+180)*(Math.PI/180),
        x = -((radius) * Math.sin(phi)*Math.cos(theta)),
        z = ((radius) * Math.sin(phi)*Math.sin(theta)),
        y = ((radius) * Math.cos(phi));
    return new THREE.Vector3(x,y,z);
}